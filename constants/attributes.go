package constants

const SchemaVersion = "schemaVersion"

const Name = "name"
const LastRead = "last-read"
const Picture = "picture"
const DefaultProfilePicture = "defaultPicture"
const ShowBlocked = "show-blocked"
const Archived = "archived"
const LastSeenTime = "lastMessageSeenTime"

const ProfileTypeV1DefaultPassword = "v1-defaultPassword"
const ProfileTypeV1Password = "v1-userPassword"

// PeerOnline stores state on if the peer believes it is online
const PeerOnline = "peer-online"

const PeerAutostart = "autostart"

// Description is used on server contacts,
const Description = "description"

// ConversationNotificationPolicy is the attribute label for conversations. When App NotificationPolicy is OptIn a true value here opts in
const ConversationNotificationPolicy = "notification-policy"

const StateProfilePane = "state-profile-pane"
const StateSelectedConversation = "state-selected-conversation"
const StateSelectedProfileTime = "state-selected-profile-time"

// Settings
const BlockUnknownPeersSetting = "blockunknownpeers"
const LocaleSetting = "locale"
const ZoomSetting = "zoom"

// App Experiments
const MessageFormattingExperiment = "message-formatting"
